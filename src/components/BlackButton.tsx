import React from 'react'
import { StyleProp, StyleSheet, Text, TouchableOpacity, ViewStyle } from 'react-native';

interface Props {
  title: string;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
}

export const BlackButton = ({title, onPress, style = {}}: Props) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.9}
      style={{
        ...style as any,
        ...styles.blackButton,
      }}
    >
      <Text style={styles.buttonTxt}>{title}</Text>
    </TouchableOpacity>
  );
};

//create button style react native
const styles = StyleSheet.create({
  blackButton : {
    height: 45,
    width: 200,
    backgroundColor: 'black',
    justifyContent: 'center',
    borderRadius: 50,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    elevation: 6,
  },
  buttonTxt: {
    color: 'white',
    fontSize: 18,
  },
});
