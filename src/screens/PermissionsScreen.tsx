import React, { useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { BlackButton } from '../components/BlackButton';
import { PermissionsContext } from '../context/PermissionsContext';

export const PermissionsScreen = () => {

  const {permissions, askLocationPermission} = useContext(PermissionsContext);

  return (
    <View style={styles.container}>
      <Text style={styles.title}> Es necesario el uso del GPS para usar esta aplicacion </Text>

      <BlackButton
        title="Permiso"
        onPress={askLocationPermission}
      />

      <Text>
        {JSON.stringify(permissions, null, 5)}
      </Text>
    </View>
  );
  };

const styles = StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    title: {
      width: 200,
      fontSize: 18,
      textAlign: 'center',
      marginBottom: 20,
    }
});
